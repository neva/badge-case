// t = 3
// h = 86
// w = 54

// 1.68 back
//

// rsa top hook 4mm
// 27.7 circ
// 20.4 body rect
// 58.12 long
// 10mm thick

//  top 2.57 from back to hook
// front to hook 3.45

cardDepth = 3; //2;
cardHeight = 86; //120.0;
cardWidth = 54; //75.0;
base = [cardDepth, cardWidth, cardHeight];
thick = 2;
radius = 2;
safe = 0;
RES = 50;
tol = 0.05;    
x = base[1];
y = base[0];
z = base[2];
sx = x + (safe * 2);
sy = y + (safe * 2);
sz = z + (safe);
tx = sx + (thick * 2);
ty = sy + (thick * 3);
tz = sz + (thick);
t = thick;
dt = t * 2;
cyHeight = 10;
cyRadius = (27.7/2)+dt;
rsaCube = [10, 20+dt+t, 39];
align = [0, -(tx/2), -(tz/2)];

module card() {    
    radius = 1;
    translate([thick, align[1] + thick, align[2] + thick])
    #cube(base);
    // minkowski() {
    //     cube(base);
    //     sphere(r=radius);
    // }
}

module case(cardSize, tolerance, thickness) {    
    translate(align)
    difference() {
        cube([ty, tx, tz]);
        union() {
            translate([t, t, t+tol]) {
                cube([sy + t, sx, sz]);
                translate([0, t, tolerance])
                cube([sy + (thickness * 2) + tol, sx - (thickness * 2), sz]);
            }
        }
    }
}

module rsa() {
    badgeCube = [rsaCube[0], rsaCube[1], rsaCube[2] + dt];
    
    translate(align)
    translate([-cyHeight, (tx)/2, base[2]/2]) {
        union() {
            rotate([0, 90, 0]) 
            cylinder(cyHeight, cyRadius, cyRadius, $fn = RES);  
            rotate([180, 0, 0])
            translate([0, -(badgeCube[1]/2), 0])
            cube(badgeCube);
        }
    }
}

module rsa_cutout() {
    cy_align = [-cyHeight, tx/2, base[2]/2];
    mcw = rsaCube[1] - dt;
    
    translate(align) union() {
        translate(cy_align) {
            rotate([0, 90, 0]) {
                translate([0, 0, t])
                cylinder(cyHeight+dt, cyRadius - thick, cyRadius - thick, $fn=RES);
                translate([0, 0, -dt])
                cylinder(dt+t, cyRadius - t, cyRadius - dt, $fn=RES);
            }
            rotate([0, 180, 180]) {                
                translate([t, -mcw/2, 0])
                cube([rsaCube[0]-dt+dt+dt, mcw, rsaCube[2]+t]);
                translate([-dt, -(mcw - dt)/2, 0])
                cube([dt+t, mcw - dt, rsaCube[2]]);            
            }
            translate([6, 0, cyRadius-(2*dt)])
            cylinder(dt*2, 1, dt, $fn=50);
        }
    }
}


module badge() {
    difference() {
        union() {
            case(base, safe, thick); 
            rsa(safe, thick, 0);
        }
        rsa_cutout(safe, thick);
    }
}

//card();
badge();
//#translate([-10+t, -20.4/2, -41.5]) cube([10, 20.4, 58.2]);
